package teaiwari

import (
	"bitbucket.org/dchiba/chess/koma"
)

var Hirate Teaiwari = Teaiwari{
	Position{0, 0, koma.ROOK, true},
	Position{1, 0, koma.KNIGHT, true},
	Position{2, 0, koma.BISHOP, true},
	Position{3, 0, koma.QUEEN, true},
	Position{4, 0, koma.KING, true},
	Position{5, 0, koma.BISHOP, true},
	Position{6, 0, koma.KNIGHT, true},
	Position{7, 0, koma.ROOK, true},

	Position{0, 1, koma.PAWN, true},
	Position{1, 1, koma.PAWN, true},
	Position{2, 1, koma.PAWN, true},
	Position{3, 1, koma.PAWN, true},
	Position{4, 1, koma.PAWN, true},
	Position{5, 1, koma.PAWN, true},
	Position{6, 1, koma.PAWN, true},
	Position{7, 1, koma.PAWN, true},

	Position{0, 6, koma.PAWN, false},
	Position{1, 6, koma.PAWN, false},
	Position{2, 6, koma.PAWN, false},
	Position{3, 6, koma.PAWN, false},
	Position{4, 6, koma.PAWN, false},
	Position{5, 6, koma.PAWN, false},
	Position{6, 6, koma.PAWN, false},
	Position{7, 6, koma.PAWN, false},

	Position{0, 7, koma.ROOK, false},
	Position{1, 7, koma.KNIGHT, false},
	Position{2, 7, koma.BISHOP, false},
	Position{3, 7, koma.QUEEN, false},
	Position{4, 7, koma.KING, false},
	Position{5, 7, koma.BISHOP, false},
	Position{6, 7, koma.KNIGHT, false},
	Position{7, 7, koma.ROOK, false},
}
