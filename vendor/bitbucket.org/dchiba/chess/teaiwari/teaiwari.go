package teaiwari

import "bitbucket.org/dchiba/chess/koma"

type Position struct {
	X, Y  int
	Type  koma.Type
	Uwate bool
}

type Teaiwari []Position
