package teaiwari

import (
	"bitbucket.org/dchiba/chess/koma"
)

var Mini Teaiwari = Teaiwari{
	Position{0, 0, koma.ROOK, true},
	Position{1, 0, koma.KNIGHT, true},
	Position{2, 0, koma.BISHOP, true},
	Position{3, 0, koma.KING, true},
	Position{4, 0, koma.QUEEN, true},

	Position{0, 1, koma.PAWN, true},
	Position{1, 1, koma.PAWN, true},
	Position{2, 1, koma.PAWN, true},
	Position{3, 1, koma.PAWN, true},
	Position{4, 1, koma.PAWN, true},

	Position{0, 4, koma.PAWN, false},
	Position{1, 4, koma.PAWN, false},
	Position{2, 4, koma.PAWN, false},
	Position{3, 4, koma.PAWN, false},
	Position{4, 4, koma.PAWN, false},

	Position{0, 5, koma.QUEEN, false},
	Position{1, 5, koma.KING, false},
	Position{2, 5, koma.BISHOP, false},
	Position{3, 5, koma.KNIGHT, false},
	Position{4, 5, koma.ROOK, false},
}
