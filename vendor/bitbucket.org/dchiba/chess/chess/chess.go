package chess

import (
	myapptime "bitbucket.org/dchiba/chess/apptime"
	"bitbucket.org/dchiba/chess/ban"
	"bitbucket.org/dchiba/chess/command"
	"bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"bitbucket.org/dchiba/chess/teaiwari"
	"errors"
	"github.com/nirasan/apptime"
	"time"
)

type Chess struct {
	Mode    Mode
	Player1 *player.Player
	Player2 *player.Player
	Ban     *ban.Ban
	Status  Status
	Winner  *player.Player
}

type Mode int8

const (
	DefaultMode Mode = iota
	MiniMode
	MicroMode
)

type Status int8

const (
	Preparing Status = iota
	Playing
	Finished
)

var (
	NotPlaying    = errors.New("Chess is not playing")
	InvalidPlayer = errors.New("Invalid player")
)

func New(mode Mode) *Chess {
	var b *ban.Ban
	switch mode {
	case DefaultMode:
		b = ban.NewDefaultBan()
	case MiniMode:
		b = ban.NewMiniBan()
	case MicroMode:
		b = ban.NewMicroBan()
	default:
		panic("invalid mode")
	}
	return &Chess{
		Mode:   mode,
		Ban:    b,
		Status: Preparing,
	}
}

func (s *Chess) Players() []*player.Player {
	players := []*player.Player{}
	if s.Player1 != nil {
		players = append(players, s.Player1)
	}
	if s.Player2 != nil {
		players = append(players, s.Player2)
	}
	return players
}

func (s *Chess) SetPlayer(p *player.Player) error {
	if s.Player1 == nil {
		s.Player1 = p
	} else if s.Player2 == nil {
		s.Player2 = p
	} else {
		return errors.New("too many player")
	}
	return nil
}

func (s *Chess) HasPlayer(p *player.Player) bool {
	return s.Player1 == p || s.Player2 == p
}

func (s *Chess) IsPlaying() bool {
	return s.Status == Playing
}

func (s *Chess) Prepared() bool {
	return s.Status == Preparing && s.Player1 != nil && s.Player2 != nil
}

func (s *Chess) Initialize() {
	if s.Prepared() {
		s.Status = Playing
		var t teaiwari.Teaiwari
		switch s.Mode {
		case DefaultMode:
			t = teaiwari.Hirate
		case MiniMode:
			t = teaiwari.Mini
		case MicroMode:
			t = teaiwari.Micro
		default:
			panic("invalid mode")
		}
		command.Arrange(s.Player2, s.Player1, s.Ban, t)
	} else {
		panic("invalid status")
	}
}

func (s *Chess) MovablePositions(p *player.Player, pos *ban.Position) (command.MovePositions, error) {
	if !s.IsPlaying() {
		return nil, NotPlaying
	}
	if !s.HasPlayer(p) {
		return nil, InvalidPlayer
	}
	return command.MovablePositions(p, s.Ban, pos)
}

func (s *Chess) Move(p *player.Player, from *ban.Position, to *ban.Position, nari bool) (*ban.Koma, *ban.Koma, *ban.Koma, error) {
	if !s.IsPlaying() {
		return nil, nil, nil, NotPlaying
	}
	if !s.HasPlayer(p) {
		return nil, nil, nil, InvalidPlayer
	}
	moved, got, castled, err := command.Move(p, s.Ban, from, to, nari)
	if got != nil && got.Type == koma.KING {
		s.Winner = p
		s.Status = Finished
	}
	return moved, got, castled, err
}

func (s *Chess) CpuHand(p *player.Player, level int) (*command.MovePosition, *ban.Koma, *ban.Koma, *ban.Koma, error) {
	if !s.IsPlaying() {
		return nil, nil, nil, nil, NotPlaying
	}
	if !s.HasPlayer(p) {
		return nil, nil, nil, nil, InvalidPlayer
	}
	mp, moved, got, castled, err := command.CpuHand(p, s.Ban, level)
	if got != nil && got.Type == koma.KING {
		s.Winner = p
		s.Status = Finished
	}
	return mp, moved, got, castled, err
}

func (s *Chess) GetApptime() *apptime.Apptime {
	return myapptime.GetTime()
}

func (s *Chess) SetApptime(t time.Time) {
	myapptime.SetTime(t)
}

func (s *Chess) ClearApptime() {
	myapptime.Clear()
}
