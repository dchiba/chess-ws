package koma

import (
	"fmt"
)

type Koma struct {
	ID uint8
	Type
}

func New(id uint8, t Type) *Koma {
	return &Koma{id, t}
}

func (k *Koma) Label() string {
	return k.Type.Label()
}

func (k *Koma) String() string {
	return fmt.Sprintf("Koma{ID:%d,Type:%s}", k.ID, k.Label())
}

func (k *Koma) GetMoveTypes() MoveTypes {
	return KomaMoveType[k.Type]
}
