package apptime

import (
	"github.com/nirasan/apptime"
	"time"
)

var myAppTime *apptime.Apptime

func GetTime() *apptime.Apptime {
	if myAppTime != nil {
		return myAppTime
	} else {
		return apptime.New()
	}
}

func SetTime(t time.Time) {
	a := apptime.New()
	a.Set(t)
	myAppTime = a
}

func Clear() {
	myAppTime = nil
}
