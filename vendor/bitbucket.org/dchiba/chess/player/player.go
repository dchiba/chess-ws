package player

type Player struct {
	ID    uint64
	Sente bool
}

func New(id uint64, sente bool) *Player {
	return &Player{ID: id, Sente: sente}
}
