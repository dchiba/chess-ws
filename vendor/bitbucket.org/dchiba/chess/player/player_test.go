package player

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNew(t *testing.T) {
	p := New(1, true)
	assert.NotNil(t, p)
}
