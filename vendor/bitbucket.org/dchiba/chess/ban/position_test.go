package ban

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewPosition(t *testing.T) {
	b := NewDefaultBan()
	p, e := b.NewPosition(1, 1)
	assert.NotNil(t, p, "正常に作成できている")
	assert.Nil(t, e, "正常に作成できている")
	assert.Equal(t, p.X, 1)
	assert.Equal(t, p.Y, 1)

	_, e = b.NewPosition(-1, 0)
	assert.NotNil(t, p, "範囲外は作成できない")
	_, e = b.NewPosition(0, -1)
	assert.NotNil(t, p, "範囲外は作成できない")
	_, e = b.NewPosition(9, 0)
	assert.NotNil(t, p, "範囲外は作成できない")
	_, e = b.NewPosition(0, 9)
	assert.NotNil(t, p, "範囲外は作成できない")
}

func TestPosition_Front(t *testing.T) {
	var p *Position
	var e error

	b := NewDefaultBan()

	p, e = b.NewPosition(4, 4)
	p, e = b.Front(p, true)
	if assert.Nil(t, e) {
		assert.Equal(t, p.X, 4)
		assert.Equal(t, p.Y, 3)
	}

	p, _ = b.NewPosition(4, 4)
	p, e = b.Front(p, false)
	if assert.Nil(t, e) {
		assert.Equal(t, p.X, 4)
		assert.Equal(t, p.Y, 5)
	}
}
