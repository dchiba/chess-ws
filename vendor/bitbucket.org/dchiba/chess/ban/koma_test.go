package ban

import (
	"bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewKoma(t *testing.T) {
	k := NewKoma(1, koma.KING, player.New(1, true))
	assert.NotNil(t, k)
}

func TestKoma_MovableTime(t *testing.T) {
	k := NewKoma(1, koma.KING, player.New(1, true))
	assert.NotNil(t, k.MovableTime())
}
