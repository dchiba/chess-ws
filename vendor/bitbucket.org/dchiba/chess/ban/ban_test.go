package ban

import (
	"bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNew(t *testing.T) {
	b := NewDefaultBan()
	assert.NotNil(t, b)
	assert.Equal(t, len(b.Table), b.Height)
	assert.Equal(t, len(b.Table[0]), b.Width)
	assert.Nil(t, b.Table[0][0])
	assert.Nil(t, b.Table[b.Height-1][b.Width-1])
}

func TestBan_SetKoma(t *testing.T) {
	p := player.New(1, true)
	b := NewDefaultBan()
	pos, _ := b.NewPosition(4, 0)
	b.SetKoma(pos, koma.KING, p)
	assert.NotNil(t, b.Table[0][4])
	assert.Equal(t, b.Table[0][4].Type, koma.KING)
}

func TestBan_MoveKoma(t *testing.T) {
	p1 := player.New(1, true)
	p2 := player.New(2, false)
	b := NewDefaultBan()
	k1 := b.SetKoma(&Position{4, 4}, koma.PAWN, p1)
	k2 := b.SetKoma(&Position{4, 3}, koma.PAWN, p1)

	k, e := b.MoveKoma(&Position{0, 0}, &Position{4, 3})
	assert.NotNil(t, e, "駒がない")

	k, e = b.MoveKoma(&Position{4, 4}, &Position{4, 3})
	assert.NotNil(t, e, "自分の駒がある")

	k2.Player = p2
	k, e = b.MoveKoma(&Position{4, 4}, &Position{4, 3})
	assert.Nil(t, e, "移動できる")
	assert.Equal(t, k2, k, "駒をゲットした")
	assert.Equal(t, b.Table[3][4], k1, "移動先に駒がある")
	assert.Equal(t, k1.MoveCount, 1, "移動回数が増えた")
}
