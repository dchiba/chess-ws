package ban

import (
	"bitbucket.org/dchiba/chess/apptime"
	"bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"fmt"
)

type Koma struct {
	koma.Koma
	Player           *player.Player
	IsNari           bool
	LastMoveUnixtime int64
	MoveCount        int
}

func NewKoma(id uint8, t koma.Type, p *player.Player) *Koma {
	return &Koma{koma.Koma{id, t}, p, false, apptime.GetTime().Now().Unix(), 0}
}

func (k *Koma) String() string {
	return fmt.Sprintf("{%d,%s,%d}", k.ID, k.Label(), k.Player.ID)
}

func (k *Koma) UpdateLastMoveUnixtime() {
	k.LastMoveUnixtime = apptime.GetTime().Now().Unix()
}

func (k *Koma) MovableTime() int64 {
	return k.LastMoveUnixtime + koma.WaitTimeList[k.Type]
}

func (k *Koma) IsMovableTime() bool {
	return apptime.GetTime().Now().Unix() >= k.MovableTime()
}
