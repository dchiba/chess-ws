package command

import (
	"bitbucket.org/dchiba/chess/apptime"
	"bitbucket.org/dchiba/chess/ban"
	"bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"bitbucket.org/dchiba/chess/teaiwari"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestMove(t *testing.T) {
	assert := assert.New(t)
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	Arrange(p1, p2, b, teaiwari.Teaiwari{
		teaiwari.Position{4, 2, koma.PAWN, true},
		teaiwari.Position{3, 1, koma.PAWN, false},
	})

	moved, got, _, e := Move(p1, b, &ban.Position{4, 1}, &ban.Position{4, 2}, false)
	assert.NotNil(e, "移動できない")

	moved, got, _, e = Move(p1, b, &ban.Position{4, 2}, &ban.Position{4, 1}, false)
	assert.Equal(e, errors.New("Koma is waiting time"))

	apptime.SetTime(time.Now().Add(1 * time.Minute)) // 駒の移動待ち時間のために時間をすすめる
	moved, got, _, e = Move(p1, b, &ban.Position{4, 2}, &ban.Position{3, 1}, false)
	assert.Nil(e, "移動できる")
	assert.Equal(got.Type, koma.PAWN, "ポーンをゲット")

	apptime.SetTime(time.Now().Add(2 * time.Minute)) // 駒の移動待ち時間のために時間をすすめる

	moved, got, _, e = Move(p1, b, &ban.Position{3, 1}, &ban.Position{3, 0}, true)
	assert.Nil(e, "移動できる")
	assert.Equal(moved.Type, koma.QUEEN, "なっている")
	assert.Nil(got, "何もゲットできない")
}

func TestMove2(t *testing.T) {
	assert := assert.New(t)
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	Arrange(p1, p2, b, teaiwari.Teaiwari{
		teaiwari.Position{4, 3, koma.PAWN, true},
		teaiwari.Position{3, 3, koma.PAWN, false},
	})

	k1, _ := b.GetKoma(&ban.Position{4, 3})
	k2, _ := b.GetKoma(&ban.Position{3, 3})

	k1.MoveCount = 2
	k2.MoveCount = 1

	apptime.SetTime(time.Now().Add(3 * time.Minute)) // 駒の移動待ち時間のために時間をすすめる
	moved, got, _, e := Move(p1, b, &ban.Position{4, 3}, &ban.Position{3, 2}, false)
	assert.Nil(e, "アンパッサンで移動できる")
	assert.NotNil(moved, "アンパッサンで移動できる")
	assert.Equal(got.Type, koma.PAWN, "アンパッサンで相手のポーンをゲット")

	k3, _ := b.GetKoma(&ban.Position{3, 3})
	assert.Nil(k3, "アンパッサンで取られている")
}

func TestMove3(t *testing.T) {
	assert := assert.New(t)
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	Arrange(p1, p2, b, teaiwari.Teaiwari{
		teaiwari.Position{0, 0, koma.ROOK, true},
		teaiwari.Position{4, 0, koma.KING, true},
		teaiwari.Position{7, 0, koma.ROOK, true},
		teaiwari.Position{0, 7, koma.ROOK, false},
		teaiwari.Position{4, 7, koma.KING, false},
		teaiwari.Position{7, 7, koma.ROOK, false},
	})

	apptime.SetTime(time.Now().Add(4 * time.Minute)) // 駒の移動待ち時間のために時間をすすめる

	v1, v2, castled, e := Move(p1, b, &ban.Position{4, 0}, &ban.Position{2, 0}, false)
	t.Log(v1, v2)
	if assert.Nil(e, "先手がキャスリングでクイーンサイドに移動できる") {
		k, _ := b.GetKoma(&ban.Position{3, 0})
		if assert.NotNil(k, "クイーンサイドのルークが移動している") {
			assert.Equal(castled.Type, koma.ROOK)
			assert.Equal(k.Type, koma.ROOK)
			k, _ = b.GetKoma(&ban.Position{0, 0})
			assert.Nil(k, "クイーンサイドのルークの初期位置が空になっている")
		}
	}

	_, _, castled, e = Move(p2, b, &ban.Position{4, 7}, &ban.Position{6, 7}, false)
	if assert.Nil(e, "後手がキャスリングでキングサイドに移動できる") {
		k, _ := b.GetKoma(&ban.Position{5, 7})
		if assert.NotNil(k, "キングサイドのルークが移動している") {
			assert.Equal(castled.Type, koma.ROOK)
			assert.Equal(k.Type, koma.ROOK)
			k, _ = b.GetKoma(&ban.Position{7, 7})
			assert.Nil(k, "キングサイドのルークの初期位置が空になっている")
		}

	}
}
