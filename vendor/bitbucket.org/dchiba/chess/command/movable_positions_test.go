package command

import (
	"bitbucket.org/dchiba/chess/ban"
	"bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"bitbucket.org/dchiba/chess/teaiwari"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMovablePositions(t *testing.T) {
	b := ban.NewDefaultBan()
	p1 := player.New(1, false)

	pos, _ := b.NewPosition(4, 4)
	k := b.SetKoma(pos, koma.PAWN, p1)

	sample := []struct {
		Type      koma.Type
		Positions MovePositions
	}{
		{koma.KING, MovePositions{
			movePosition(4, 5, false), // 前
			movePosition(4, 3, false), // 後
			movePosition(5, 4, false), // 右
			movePosition(3, 4, false), // 左
			movePosition(5, 5, false), // 右前
			movePosition(3, 5, false), // 左前
			movePosition(5, 3, false), // 右後
			movePosition(3, 3, false), // 左後
		}},
		{koma.QUEEN, MovePositions{
			movePosition(4, 5, false), movePosition(4, 6, false), movePosition(4, 7, false),
			movePosition(4, 3, false), movePosition(4, 2, false), movePosition(4, 1, false), movePosition(4, 0, false),
			movePosition(5, 4, false), movePosition(6, 4, false), movePosition(7, 4, false),
			movePosition(3, 4, false), movePosition(2, 4, false), movePosition(1, 4, false), movePosition(0, 4, false),
			movePosition(5, 5, false), movePosition(6, 6, false), movePosition(7, 7, false),
			movePosition(3, 3, false), movePosition(2, 2, false), movePosition(1, 1, false), movePosition(0, 0, false),
			movePosition(5, 3, false), movePosition(6, 2, false), movePosition(7, 1, false),
			movePosition(3, 5, false), movePosition(2, 6, false), movePosition(1, 7, false),
		}},
		{koma.ROOK, MovePositions{
			movePosition(4, 5, false), movePosition(4, 6, false), movePosition(4, 7, false),
			movePosition(4, 3, false), movePosition(4, 2, false), movePosition(4, 1, false), movePosition(4, 0, false),
			movePosition(5, 4, false), movePosition(6, 4, false), movePosition(7, 4, false),
			movePosition(3, 4, false), movePosition(2, 4, false), movePosition(1, 4, false), movePosition(0, 4, false),
		}},
		{koma.BISHOP, MovePositions{
			movePosition(5, 5, false), movePosition(6, 6, false), movePosition(7, 7, false),
			movePosition(3, 3, false), movePosition(2, 2, false), movePosition(1, 1, false), movePosition(0, 0, false),
			movePosition(5, 3, false), movePosition(6, 2, false), movePosition(7, 1, false),
			movePosition(3, 5, false), movePosition(2, 6, false), movePosition(1, 7, false),
		}},
		{koma.KNIGHT, MovePositions{
			movePosition(5, 6, false), // 右前前
			movePosition(3, 6, false), // 左前前
			movePosition(6, 5, false), // 右右前
			movePosition(2, 5, false), // 左左前
			movePosition(6, 3, false), // 右右後
			movePosition(2, 3, false), // 左左後
			movePosition(5, 2, false), // 右後後
			movePosition(3, 2, false), // 左後後
		}},
	}
	for _, s := range sample {
		k.Type = s.Type
		ps, e := MovablePositions(p1, b, pos)
		assert.Nil(t, e)
		assert.Equal(t, len(ps), len(s.Positions))
		for _, p := range ps {
			b := false
			index := -1
			for i, q := range s.Positions {
				if p.To.Equal(q.To) {
					b = true
					index = i
					break
				}
			}
			if assert.True(t, b, fmt.Sprintf("%s の移動先候補として %s が存在する", s.Type.Label(), p.String())) {
				assert.Equal(t, s.Positions[index].Nari, p.Nari, fmt.Sprintf("%s の %s の成判定が正しい", s.Type.Label(), p.String()))
			} else {
				t.Log(ps)
				t.Log(s.Positions)
			}
		}
	}
}

func movePosition(x int, y int, nari bool) *MovePosition {
	return &MovePosition{&ban.Position{}, &ban.Position{x, y}, nari, false, false, false}
}

func TestMovablePositions2(t *testing.T) {
	b := ban.NewMiniBan()
	p1 := player.New(1, false)

	pos, _ := b.NewPosition(2, 4)
	b.SetKoma(pos, koma.PAWN, p1)

	ps, e := MovablePositions(p1, b, pos)
	if assert.Nil(t, e, "MiniMode で後手の成りテスト") {
		assert.Equal(t, len(ps), 1)
		assert.Equal(t, ps[0].To.Y, 5)
		assert.True(t, ps[0].Nari, "MiniMode で後手だと Y:5 から成れる")
	}

	p2 := player.New(2, true)

	pos2, _ := b.NewPosition(2, 1)
	b.SetKoma(pos2, koma.PAWN, p2)
	ps2, e := MovablePositions(p2, b, pos2)
	if assert.Nil(t, e, "MiniMode で先手の成テスト") {
		assert.Equal(t, len(ps2), 1)
		assert.Equal(t, ps2[0].To.Y, 0)
		assert.True(t, ps2[0].Nari, "MiniMode で先手だと Y:0 から成れる")
	}
}

func TestMovablePositions3(t *testing.T) {
	b := ban.NewMicroBan()
	p1 := player.New(1, false)

	pos, _ := b.NewPosition(1, 2)
	b.SetKoma(pos, koma.PAWN, p1)

	ps, e := MovablePositions(p1, b, pos)
	if assert.Nil(t, e, "MicroMode で後手の成りテスト") {
		assert.Equal(t, len(ps), 1)
		assert.Equal(t, ps[0].To.Y, 3)
		assert.True(t, ps[0].Nari, "MicroMode で後手だと Y:3 から成れる")
	}

	p2 := player.New(2, true)

	pos2, _ := b.NewPosition(1, 1)
	b.SetKoma(pos2, koma.PAWN, p2)
	ps2, e := MovablePositions(p2, b, pos2)
	if assert.Nil(t, e, "MicroMode で先手の成テスト") {
		assert.Equal(t, len(ps2), 1)
		assert.Equal(t, ps2[0].To.Y, 0)
		assert.True(t, ps2[0].Nari, "MicroMode で先手だと Y:0 から成れる")
	}
}

func TestMovablePositions4(t *testing.T) {
	b := ban.NewDefaultBan()
	p1 := player.New(1, false)
	p2 := player.New(2, true)

	pos, _ := b.NewPosition(3, 1)
	b.SetKoma(pos, koma.PAWN, p1)

	ps, e := MovablePositions(p1, b, pos)
	if assert.Nil(t, e, "Pawn の初手移動") {
		assert.Equal(t, len(ps), 2)
		assert.Equal(t, ps[0].To.Y, 2)
		assert.Equal(t, ps[1].To.Y, 3)
	}

	pos2, _ := b.NewPosition(3, 3)
	b.SetKoma(pos2, koma.PAWN, p2)
	ps, e = MovablePositions(p1, b, pos)
	if assert.Nil(t, e, "Pawn の初手移動で2マス先に敵の駒") {
		assert.Equal(t, len(ps), 1)
		assert.Equal(t, ps[0].To.Y, 2)
	}

	pos3, _ := b.NewPosition(3, 2)
	b.SetKoma(pos3, koma.PAWN, p2)
	ps, e = MovablePositions(p1, b, pos)
	if assert.Nil(t, e, "Pawn の初手移動で1マス先に敵の駒") {
		assert.Equal(t, len(ps), 0)
	}

	pos4, _ := b.NewPosition(2, 2)
	b.SetKoma(pos4, koma.PAWN, p2)
	pos5, _ := b.NewPosition(4, 2)
	b.SetKoma(pos5, koma.PAWN, p2)
	ps, e = MovablePositions(p1, b, pos)
	if assert.Nil(t, e, "Pawn の斜め前に敵の駒があれば取れる") {
		assert.Equal(t, len(ps), 2)
		assert.True(t, ps[0].To.Y == 2 && ps[0].To.X == 4)
		assert.True(t, ps[1].To.Y == 2 && ps[1].To.X == 2)
	}

	pos6, _ := b.NewPosition(5, 4)
	k1 := b.SetKoma(pos6, koma.PAWN, p2)
	k1.MoveCount = 1
	pos7, _ := b.NewPosition(7, 4)
	k2 := b.SetKoma(pos7, koma.PAWN, p2)
	k2.MoveCount = 1
	pos8, _ := b.NewPosition(6, 4)
	k3 := b.SetKoma(pos8, koma.PAWN, p1)
	k3.MoveCount = 1
	ps, e = MovablePositions(p1, b, pos8)
	if assert.Nil(t, e, "Pawn の横に2コマ進んだばかりの Pawn がいたら斜め前に追い越す形で進んで取れる") {
		assert.Equal(t, len(ps), 3)
		assert.True(t, ps[0].To.Equal(&ban.Position{6, 5}))
		assert.True(t, ps[1].To.Equal(&ban.Position{7, 5}))
		assert.True(t, ps[2].To.Equal(&ban.Position{5, 5}))
		assert.False(t, ps[0].EnPassant, "通常の直進なのでアンパッサンフラグが立っていない")
		assert.True(t, ps[1].EnPassant, "アンパッサンフラグが立っている")
		assert.True(t, ps[2].EnPassant, "アンパッサンフラグが立っている")
	}
}

func TestMovablePositions5(t *testing.T) {
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	Arrange(p1, p2, b, teaiwari.Teaiwari{
		teaiwari.Position{0, 0, koma.ROOK, true},
		teaiwari.Position{4, 0, koma.KING, true},
		teaiwari.Position{7, 0, koma.ROOK, true},
		teaiwari.Position{0, 7, koma.ROOK, false},
		teaiwari.Position{4, 7, koma.KING, false},
		teaiwari.Position{7, 7, koma.ROOK, false},
	})

	ps, e := MovablePositions(p1, b, &ban.Position{4, 0})
	if assert.Nil(t, e, "先手のキャスリングテスト") {
		assert.Equal(t, len(ps), 7)
		assert.True(t, ps[5].To.Equal(&ban.Position{2, 0}), "先手クイーンサイドのキャスリング")
		assert.True(t, ps[6].To.Equal(&ban.Position{6, 0}), "先手キングサイドのキャスリング")
		assert.True(t, ps[5].Castling, "キャスリングフラグが立っている")
		assert.True(t, ps[6].Castling, "キャスリングフラグが立っている")
	}

	ps, e = MovablePositions(p2, b, &ban.Position{4, 7})
	if assert.Nil(t, e, "後手のキャスリングテスト") {
		assert.Equal(t, len(ps), 7)
		assert.True(t, ps[5].To.Equal(&ban.Position{2, 7}), "後手クイーンサイドのキャスリング")
		assert.True(t, ps[6].To.Equal(&ban.Position{6, 7}), "後手キングサイドのキャスリング")
		assert.True(t, ps[5].Castling, "キャスリングフラグが立っている")
		assert.True(t, ps[6].Castling, "キャスリングフラグが立っている")
	}

	b.SetKoma(&ban.Position{1, 0}, koma.KNIGHT, p1)
	ps, e = MovablePositions(p1, b, &ban.Position{4, 0})
	if assert.Nil(t, e, "先手のキャスリングテスト(クイーンサイドをナイトで塞がれている)") {
		assert.Equal(t, len(ps), 6, "5(前後左右斜め) + 1(キングサイドのキャスリング) で 6")
		assert.True(t, ps[5].To.Equal(&ban.Position{6, 0}), "先手キングサイドのキャスリング")
		assert.True(t, ps[5].Castling, "キャスリングフラグが立っている")
	}
}

func TestFrontPositions(t *testing.T) {
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	from, _ := b.NewPosition(4, 7)
	to, _ := b.NewPosition(4, 6)
	k := b.SetKoma(from, koma.KING, p1)
	ps, e := FrontPositions(b, k, from)
	if assert.Nil(t, e, "前に移動できる") {
		assert.Equal(t, len(ps), 1)
		assert.True(t, to.Equal(ps[0].To))
	}

	k2 := b.SetKoma(to, koma.PAWN, p1)
	ps, e = FrontPositions(b, k, from)
	assert.NotNil(t, e, "前に自分の駒があると移動できない")

	k2.Player = p2
	ps, e = FrontPositions(b, k, from)
	assert.Nil(t, e, "前にあるのが敵の駒なら移動できる")
}

func TestFrontFrontRightJumpPositions(t *testing.T) {
	b := ban.NewDefaultBan()
	p := player.New(1, true)

	from, _ := b.NewPosition(1, 7)
	to, _ := b.NewPosition(0, 5)
	k := b.SetKoma(from, koma.KNIGHT, p)
	ps, e := FrontFrontRightJumpPositions(b, k, from)

	assert.Nil(t, e, "移動できる")
	assert.Equal(t, len(ps), 1, "ジャンプしているので途中のマスは候補に出ない")
	assert.True(t, to.Equal(ps[0].To), "桂馬飛びできている")
}

func TestFrontStraightPositions(t *testing.T) {
	b := ban.NewDefaultBan()
	p := player.New(1, true)

	from, _ := b.NewPosition(1, 7)
	k := b.SetKoma(from, koma.ROOK, p)
	ps, e := FrontStraightPositions(b, k, from)

	assert.Nil(t, e, "移動できる")
	assert.Equal(t, len(ps), 7, "前方の全てのマスの候補がある")
	for i, pos := range []*MovePosition{
		movePosition(1, 6, false),
		movePosition(1, 5, false),
		movePosition(1, 4, false),
		movePosition(1, 3, false),
		movePosition(1, 2, false),
		movePosition(1, 1, false),
		movePosition(1, 0, false),
	} {
		assert.True(t, ps[i].To.Equal(pos.To), "前方のすべてのマスの候補がある")
	}

	p2 := player.New(2, false)
	b.SetKoma(&ban.Position{1, 1}, koma.PAWN, p2)
	ps, e = FrontStraightPositions(b, k, from)

	//for _, pos1 := range ps {
	//	t.Log(pos1)
	//}

	assert.Nil(t, e, "前方に敵の駒があっても移動できる")
	assert.Equal(t, len(ps), 6, "前方の敵の駒のマスまでの候補がある")
	for i, pos := range []*MovePosition{
		movePosition(1, 6, false),
		movePosition(1, 5, false),
		movePosition(1, 4, false),
		movePosition(1, 3, false),
		movePosition(1, 2, false),
		movePosition(1, 1, false),
	} {
		assert.True(t, ps[i].To.Equal(pos.To), "前方の敵の駒のマスまでの候補がある")
	}

	b.SetKoma(&ban.Position{1, 4}, koma.PAWN, p)
	ps, e = FrontStraightPositions(b, k, from)

	assert.Nil(t, e, "前方に自分の駒があっても移動できる")
	assert.Equal(t, len(ps), 2, "前方の自分の駒のマスの前までの候補がある")
	for i, pos := range []*MovePosition{
		movePosition(1, 6, false),
		movePosition(1, 5, false),
	} {
		assert.True(t, ps[i].To.Equal(pos.To), "前方の自分の駒のマスの前までの候補がある")
	}
}
