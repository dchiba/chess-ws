package command

import (
	"bitbucket.org/dchiba/chess/ban"
	"bitbucket.org/dchiba/chess/player"
	"errors"
	"math/rand"
	"time"
)

func CpuHand(p *player.Player, b *ban.Ban, level int) (mp *MovePosition, moved *ban.Koma, got *ban.Koma, castled *ban.Koma, e error) {

	// 全ての駒を検索して自分の駒で動かせるもののリストをつくる
	movePositions := MovePositions{}
	for x := 0; x < b.Width; x++ {
		for y := 0; y < b.Height; y++ {
			pos := &ban.Position{x, y}
			k, e := b.GetKoma(pos)
			if e != nil || k == nil || k.Player.ID != p.ID || !k.IsMovableTime() {
				continue
			}
			// MovePositions を使って動かせられる手を検索する
			ps, e := MovablePositions(p, b, pos)
			if e != nil {
				continue
			}
			movePositions = append(movePositions, ps...)
		}
	}

	if len(movePositions) == 0 {
		return nil, nil, nil, nil, errors.New("not found")
	}

	// ランダムに動かす手を抽選
	rand.Seed(time.Now().UnixNano())
	if level >= 0 {
		mp = movePositions[rand.Intn(len(movePositions))]
	} else {
		//TODO 手の中から優先度に従った重み付きランダムで実際に動かす手を抽選
	}

	// Move を使って動かす
	moved, got, castled, e = Move(p, b, mp.From, mp.To, mp.Nari)
	return mp, moved, got, castled, e
}
