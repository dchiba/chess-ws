package command

import (
	"bitbucket.org/dchiba/chess/ban"
	"bitbucket.org/dchiba/chess/player"
	"errors"
)

func Move(p *player.Player, b *ban.Ban, from *ban.Position, to *ban.Position, nari bool) (moved *ban.Koma, got *ban.Koma, castled *ban.Koma, e error) {
	// 駒があるか
	k, e := b.GetKoma(from)
	if e != nil {
		e = KomaNotFoundError
		return
	}
	if k.Player.ID != p.ID {
		e = KomaNotOwnedError
		return
	}

	// 移動可能かどうか
	if !k.IsMovableTime() {
		e = errors.New("Koma is waiting time")

		return
	}
	positions, e := MovablePositions(p, b, from)
	if e != nil {
		e = PositionNotFoundError
		return
	}
	var movePosition *MovePosition
	for _, mp := range positions {
		if mp.To.Equal(to) {
			movePosition = mp
		}
	}
	if movePosition == nil {
		e = PositionNotFoundError
		return
	}

	// 成れるかどうか
	if nari && (!movePosition.Nari || !k.CanNari()) {
		e = errors.New("Koma is not able to NARI")
		return
	}

	// 移動
	got, e = b.MoveKoma(from, to)
	if e != nil {
		return
	}
	moved = k

	// 成る
	if nari {
		newType, err := k.Nari()
		if err != nil {
			e = err
			return
		}
		k.Type = newType
		moved = k
	}

	// アンパッサン
	if movePosition.EnPassant {
		if back, err := b.Back(to, p.Sente); err == nil {
			backKoma, _ := b.GetKoma(back)
			if backKoma != nil && p.ID == backKoma.Player.ID {
				e = errors.New("Target is mine")
				return
			}
			b.Table[back.Y][back.X] = nil
			got = backKoma
		}
	}

	// キャスリング
	if movePosition.Castling {
		switch to.X {
		case 2:
			_, e = b.MoveKoma(&ban.Position{0, to.Y}, &ban.Position{3, to.Y})
			if e != nil {
				return
			}
			castled, _ = b.GetKoma(&ban.Position{3, to.Y})
		case 6:
			_, e = b.MoveKoma(&ban.Position{7, to.Y}, &ban.Position{5, to.Y})
			if e != nil {
				return
			}
			castled, _ = b.GetKoma(&ban.Position{5, to.Y})
		}
	}

	return
}
