
# Initialize

```
SERVER | CLIENT1 | CLIENT2
       <- join
join  ->

       <-          join
join            ->

initialize
       =>========>
       <- initialized
       <-          initialized
start  =>========>
```

# Play

```
SERVER | CLIENT1 | CLIENT2
       <- movable_positions
movable_positions
      ->

       <- put_koma
put_koma
      =>=========>

       <- move
move  =>=========>

finish
      =>=========>
```