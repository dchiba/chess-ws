package playerid

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewPlayerId(t *testing.T) {
	assert.Equal(t, NewPlayerId(), uint64(1))
	assert.Equal(t, NewPlayerId(), uint64(2))
	assert.Equal(t, NewPlayerId(), uint64(3))
}
