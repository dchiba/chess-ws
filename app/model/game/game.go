package game

import (
	"bitbucket.org/dchiba/chess/chess"
	"bitbucket.org/dchiba/chess/player"
)

type Game struct {
	Chess           *chess.Chess
	initializedList []bool
	IsCpu           bool
}

func NewGame(mode chess.Mode) *Game {
	return &Game{
		Chess:           chess.New(mode),
		initializedList: make([]bool, 2, 2),
		IsCpu:           false,
	}
}

func (g *Game) Status() chess.Status {
	return g.Chess.Status
}

func (g *Game) SetPlayer(p *player.Player) error {
	if len(g.Chess.Players()) == 0 {
		p.Sente = true
	} else {
		p.Sente = false
	}
	err := g.Chess.SetPlayer(p)
	if err != nil {
		return err
	}
	return nil
}

func (g *Game) AddInitializedPlayer(p *player.Player) {
	if p.ID == g.Chess.Player1.ID {
		g.initializedList[0] = true
	} else {
		g.initializedList[1] = true
	}
}

func (g *Game) ClientInitialized() bool {
	count := 0
	for _, b := range g.initializedList {
		if b {
			count++
		}
	}
	expectedCount := 2
	if g.IsCpu {
		expectedCount = 1
	}
	return count == expectedCount
}

func Int2Mode(i int) chess.Mode {
	switch i {
	case 0:
		return chess.DefaultMode
	case 1:
		return chess.MiniMode
	case 2:
		return chess.MicroMode
	default:
		panic("invalid mode")
	}
}
