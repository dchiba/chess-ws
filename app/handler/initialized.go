package handler

import (
	"bitbucket.org/dchiba/chess-ws/app/connection"
)

func Initialized(conn *connection.Connection, data *EmptyRequest) {
	if g, ok := conn.GetGame(); ok {
		g.AddInitializedPlayer(conn.Player)
		if g.ClientInitialized() {
			now := g.Chess.GetApptime().Now().Unix()
			conn.RoomEmit("start", &StartResponse{
				CurrentTime: now,
				StartTime:   now + 2,
			})
		}
	}
}
