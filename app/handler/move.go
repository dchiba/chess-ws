package handler

import (
	"bitbucket.org/dchiba/chess-ws/app/connection"
	"bitbucket.org/dchiba/chess/chess"
)

func Move(conn *connection.Connection, data *MoveRequest) {
	g, ok := conn.GetGame()
	if !ok {
		conn.Emit("error", &ErrorResponse{Msg: "game is not exist"})
	}
	p := conn.Player
	b := g.Chess.Ban
	from, err := b.NewPosition(data.From.X, data.From.Y)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	to, err := b.NewPosition(data.To.X, data.To.Y)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	moved, got, castled, err := g.Chess.Move(p, from, to, data.Nari)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	res := &MoveResponse{
		From:      data.From,
		To:        data.To,
		Player:    NewPlayerInfo(p),
		MovedKoma: NewKomaInfo(moved, to.X, to.Y),
	}
	if got != nil {
		res.Captured = true
		res.RemovedKoma = NewKomaInfo(got, to.X, to.Y)
	}
	conn.RoomEmit("move", res)

	if castled != nil {
		res2 := &MoveResponse{
			Player: NewPlayerInfo(p),
		}
		switch to.X {
		case 2:
			res2.From = PositionInfo{X: 0, Y: to.Y}
			res2.To = PositionInfo{X: 3, Y: to.Y}
			res2.MovedKoma = NewKomaInfo(castled, 3, to.Y)
		case 6:
			res2.From = PositionInfo{X: 7, Y: to.Y}
			res2.To = PositionInfo{X: 5, Y: to.Y}
			res2.MovedKoma = NewKomaInfo(castled, 7, to.Y)
		}
		conn.RoomEmit("move", res2)
	}

	if g.Status() == chess.Finished {
		conn.RoomEmit("finish", &FinishResponse{Winner: NewPlayerInfo(g.Chess.Winner)})
	}
}
