package handler

import (
	"github.com/stretchr/testify/assert"
	"log"
	"testing"
)

func TestCpuJoin(t *testing.T) {

	// 一人目作成
	conn := NewTestContexts()

	// 一人目参加
	CpuJoin(conn, &CpuJoinRequest{Mode: 0})

	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "join", "receive join message")

	myroomid := conn.RoomEmiter.(*TestRoomEmiter).Room
	assert.NotNil(t, myroomid, "入室した")
	log.Println(myroomid)

	g, _ := conn.GetGame()
	assert.NotNil(t, g, "game created")
	assert.Equal(t, len(g.Chess.Players()), 2, "player created")

	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "initialize", "send init message")
	assert.NotNil(t, conn.RoomEmiter.(*TestRoomEmiter).Data, "メンバーが揃って初期化通知は発行された")
}
