package handler

import (
	"bitbucket.org/dchiba/chess-ws/app/connection"
)

func MovablePositions(conn *connection.Connection, data *MovablePositionsRequest) {
	g, ok := conn.GetGame()
	if !ok {
		conn.Emit("error", &ErrorResponse{Msg: "game is not exist"})
	}
	p := conn.Player

	position, err := g.Chess.Ban.NewPosition(data.Position.X, data.Position.Y)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	positions, err := g.Chess.MovablePositions(p, position)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	movePositions := []MovePositionInfo{}
	for _, pos := range positions {
		movePositions = append(movePositions, MovePositionInfo{
			Position: PositionInfo{X: pos.To.X, Y: pos.To.Y},
			Nari:     pos.Nari,
		})
	}
	conn.Emit("movable_positions", &MovablePositionResponse{
		Positions: movePositions,
	})
}
