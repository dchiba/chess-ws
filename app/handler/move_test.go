package handler

import (
	"github.com/stretchr/testify/assert"
	"log"
	"testing"
)

func TestMove(t *testing.T) {
	roomid := "movetest"

	// プレイヤー参加
	conn := NewTestContexts()
	Join(conn, &JoinRequest{Name: roomid})
	resJoin := conn.Emiter.(*TestEmiter).Data.(*JoinResponse)
	player1 := resJoin.Player
	conn2 := NewTestContexts()
	Join(conn2, &JoinRequest{Name: roomid})
	resJoin = conn2.Emiter.(*TestEmiter).Data.(*JoinResponse)

	// 初期化終了
	Initialized(conn, &EmptyRequest{})
	Initialized(conn2, &EmptyRequest{})

	Move(conn, &MoveRequest{
		From: PositionInfo{X: 4, Y: 6},
		To:   PositionInfo{X: 4, Y: 7},
		Nari: false,
	})

	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "error")
	assert.Equal(t, conn.Emiter.(*TestEmiter).Data.(*ErrorResponse).Msg, "Koma is waiting time")

	g, _ := conn.GetGame()
	SetMovableTime(g, 4, 6)

	Move(conn, &MoveRequest{
		From: PositionInfo{X: 4, Y: 6},
		To:   PositionInfo{X: 4, Y: 7},
		Nari: false,
	})

	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "error")
	assert.Equal(t, conn.Emiter.(*TestEmiter).Data.(*ErrorResponse).Msg, "Posiiton not found")

	Move(conn, &MoveRequest{
		From: PositionInfo{X: 4, Y: 6},
		To:   PositionInfo{X: 4, Y: 5},
		Nari: false,
	})

	// ポーンを動かす
	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "move")
	res := conn.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
	assert.Equal(t, res.Player.Id, player1.Id)
	assert.Equal(t, res.From.X, 4)
	assert.Equal(t, res.From.Y, 6)
	assert.Equal(t, res.To.X, 4)
	assert.Equal(t, res.To.Y, 5)
	assert.False(t, res.Captured)

	// ポーンを動かし続ける
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 5)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 5}, To: PositionInfo{X: 4, Y: 4}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 4)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 4}, To: PositionInfo{X: 4, Y: 3}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 3)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 3}, To: PositionInfo{X: 4, Y: 2}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 2)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 2}, To: PositionInfo{X: 3, Y: 1}, Nari: false})

	// 相手のポーンが取れたことの確認
	res = conn.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "move")
	assert.True(t, res.Captured)
	assert.Equal(t, res.RemovedKoma.KomaBase.KomaType, uint8(6), "ポーンが盤上から取り除かれた")

	// ポーンが相手のキングにとられる
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 0)
	Move(conn2, &MoveRequest{From: PositionInfo{X: 4, Y: 0}, To: PositionInfo{X: 3, Y: 1}, Nari: false})
	res = conn.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)

	// ポーンが相手のキングに取られたことの確認
	res = conn2.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
	assert.Equal(t, conn2.RoomEmiter.(*TestRoomEmiter).Msg, "move")
	assert.True(t, res.Captured)
	assert.Equal(t, res.RemovedKoma.KomaBase.KomaType, uint8(6), "ポーンが盤上から取り除かれた")
	assert.Equal(t, res.MovedKoma.KomaBase.KomaType, uint8(1), "キングが動いた")

	// ポーンで相手のキングを取る
	for i := 6; i >= 3; i-- {
		g, _ = conn.GetGame()
		SetMovableTime(g, 2, i)
		Move(conn, &MoveRequest{From: PositionInfo{X: 2, Y: i}, To: PositionInfo{X: 2, Y: i - 1}, Nari: false})
		res = conn.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
		log.Println(res)
	}
	g, _ = conn.GetGame()
	SetMovableTime(g, 2, 2)
	Move(conn, &MoveRequest{From: PositionInfo{X: 2, Y: 2}, To: PositionInfo{X: 3, Y: 1}, Nari: false})

	// ポーンで相手のキングを取れたことの確認
	res2 := conn.RoomEmiter.(*TestRoomEmiter).Data.(*FinishResponse)
	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "finish")
	assert.Equal(t, res2.Winner.Id, player1.Id)
}

func TestMove2(t *testing.T) {
	roomid := "movetest2"

	// プレイヤー参加
	conn := NewTestContexts()
	Join(conn, &JoinRequest{Name: roomid})
	resJoin := conn.Emiter.(*TestEmiter).Data.(*JoinResponse)
	player1 := resJoin.Player
	t.Log(player1)
	conn2 := NewTestContexts()
	Join(conn2, &JoinRequest{Name: roomid})
	resJoin = conn2.Emiter.(*TestEmiter).Data.(*JoinResponse)

	// 初期化終了
	Initialized(conn, &EmptyRequest{})
	Initialized(conn2, &EmptyRequest{})

	g, _ := conn.GetGame()
	SetMovableTime(g, 6, 6)
	Move(conn, &MoveRequest{From: PositionInfo{X: 6, Y: 6}, To: PositionInfo{X: 6, Y: 4}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 6, 7)
	Move(conn, &MoveRequest{From: PositionInfo{X: 6, Y: 7}, To: PositionInfo{X: 5, Y: 5}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 5, 7)
	Move(conn, &MoveRequest{From: PositionInfo{X: 5, Y: 7}, To: PositionInfo{X: 6, Y: 6}, Nari: false})

	// キャスリング実行
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 7)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 7}, To: PositionInfo{X: 6, Y: 7}, Nari: false})

	res := conn.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "move")
	assert.False(t, res.Captured)
	assert.Equal(t, res.MovedKoma.KomaBase.KomaType, uint8(3), "キャスリングでルークが動いた")
	assert.Equal(t, res.From.X, 7)
	assert.Equal(t, res.From.Y, 7)
	assert.Equal(t, res.To.X, 5)
	assert.Equal(t, res.To.Y, 7)
}
