package handler

import (
	"bitbucket.org/dchiba/chess-ws/app/model/game"
	"bitbucket.org/dchiba/chess-ws/app/model/playerid"
	"bitbucket.org/dchiba/chess/player"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewStateInfo(t *testing.T) {
	g := game.NewGame(game.Int2Mode(0))
	g.SetPlayer(player.New(playerid.NewPlayerId(), true))
	g.SetPlayer(player.New(playerid.NewPlayerId(), true))
	g.Chess.Initialize()

	s := NewStateInfo(g)
	assert.NotNil(t, s)
}
