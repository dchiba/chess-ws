package handler

import (
	"bitbucket.org/dchiba/chess-ws/app/connection"
	"bitbucket.org/dchiba/chess-ws/app/model/game"
	"bitbucket.org/dchiba/chess-ws/app/model/playerid"
	"bitbucket.org/dchiba/chess/player"
	"fmt"
	"strconv"
	"time"
)

func CpuJoin(conn *connection.Connection, data *CpuJoinRequest) {
	fmt.Printf("Joining %v\n", data)
	// room の作成
	name := strconv.FormatInt(time.Now().UnixNano(), 10)
	conn.Join(name)
	// ゲームの作成
	_, ok := conn.GetGame()
	if ok {
		conn.Emit("error", &ErrorResponse{Msg: "invalid mode"})
		return
	}
	g := game.NewGame(game.Int2Mode(data.Mode))
	g.IsCpu = true
	conn.SetGame(g)

	// プレイヤーの作成
	p := player.New(playerid.NewPlayerId(), true)
	if err := g.SetPlayer(p); err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	conn.Player = p
	// プレイヤーの通知
	conn.Emit("join", &JoinResponse{Player: NewPlayerInfo(p)})

	// CPU プレイヤーの作成
	p2 := player.New(playerid.NewPlayerId(), true)
	if err := g.SetPlayer(p2); err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}

	// 開始通知
	g.Chess.Initialize()
	conn.RoomEmit("initialize", &InitializeResponse{
		Mode:   int(g.Chess.Mode),
		Width:  g.Chess.Ban.Width,
		Height: g.Chess.Ban.Height,
		Players: []PlayerInfo{
			NewPlayerInfo(g.Chess.Player1),
			NewPlayerInfo(g.Chess.Player2),
		},
		State: NewStateInfo(g),
	})
}
