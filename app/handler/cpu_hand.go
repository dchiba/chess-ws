package handler

import (
	"bitbucket.org/dchiba/chess-ws/app/connection"
	"bitbucket.org/dchiba/chess/chess"
	"bitbucket.org/dchiba/chess/player"
)

func CpuHand(conn *connection.Connection, data *CpuHandRequest) {
	g, ok := conn.GetGame()
	if !ok {
		conn.Emit("error", &ErrorResponse{Msg: "game is not exist"})
	}
	// get cpu player
	var p *player.Player
	for _, pp := range g.Chess.Players() {
		if pp.ID != conn.Player.ID {
			p = pp
		}
	}

	mp, moved, got, castled, err := g.Chess.CpuHand(p, data.Level)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	res := &MoveResponse{
		From:      PositionInfo{mp.From.X, mp.From.Y},
		To:        PositionInfo{mp.To.X, mp.To.Y},
		Player:    NewPlayerInfo(p),
		MovedKoma: NewKomaInfo(moved, mp.To.X, mp.To.Y),
	}
	if got != nil {
		res.Captured = true
		res.RemovedKoma = NewKomaInfo(got, mp.To.X, mp.To.Y)
	}
	conn.RoomEmit("move", res)

	if castled != nil {
		res2 := &MoveResponse{
			Player: NewPlayerInfo(moved.Player),
		}
		switch mp.To.X {
		case 2:
			res2.From = PositionInfo{X: 0, Y: mp.To.Y}
			res2.To = PositionInfo{X: 3, Y: mp.To.Y}
			res2.MovedKoma = NewKomaInfo(castled, 3, mp.To.Y)
		case 6:
			res2.From = PositionInfo{X: 7, Y: mp.To.Y}
			res2.To = PositionInfo{X: 5, Y: mp.To.Y}
			res2.MovedKoma = NewKomaInfo(castled, 7, mp.To.Y)
		}
		conn.RoomEmit("move", res2)
	}

	if g.Status() == chess.Finished {
		conn.RoomEmit("finish", &FinishResponse{Winner: NewPlayerInfo(g.Chess.Winner)})
	}
}
