package handler

import (
	"bitbucket.org/dchiba/chess-ws/app/connection"
	"fmt"
	"time"
)

func DebugSetTime(conn *connection.Connection, data *DebugSetTimeRequest) {
	fmt.Printf("Debug Set Time: %d\n", data.Time)
	if g, ok := conn.GetGame(); ok {
		g.Chess.SetApptime(time.Unix(data.Time, 0))
	}
}
